/*
	What is Data Model?

		- A data model describes how data is organized and grouped in database.

		- By creating data models, we can anticipate which data will be manage by the Database Management Sytem in accordance to the application to be Develop.

	Data Modelling 

		- Database should have a purposes and its organization must be related to the kind of application we are building.

	Scenario:
		A course booking system application where a user can enroll into a course.

	Type: Course Booking System (Web App)
	Description: A course booking system application where a user can enroll into a course.

	Features:
		- User Login (User Authentication)
		- User Registration

		Customer/Authenticated Users:
			- View Courses (all active courses)
			- Enroll Course
		
		Admin Users:
			- Add Course
			- Update Course
			- Archive/Unarchive a course (soft delete/reactivate the course)
			- View Courses (All courses active/inactive)
			- View/Manage User Account

		All Users (guest, customers, admin)
			- View Active Courses

*/

/*

	Data Models
		- Blueprints for our documents that we can follow and structure our data.
		- Show the relationship/s between our data.

*/

	User {
		id - unique identifier for the document, //system generated
		username - string,
		firstName - string,
		lastName - string,
		email - string,
		password - string,
		mobileNumber - string,
		isAdmin - boolean //no need to fillout by the customer, and it will be set to "false"
	}

	Course {
		id - unique identifier for the document,
		name - string,
		description - string,
		price - number,
		slots - number,
		schedule - dateTime/string,
		instructor - string,
		isActive - boolean //true - the is currently active and offered.
	}

	Enrollment {
		id - unique identifier for the document,
		userId - the unique identifier for the user (from the User document),
		username - string (optional),
		courseId - the unique identifier for the course (from the Course document),
		courseName - string (optional),
		isPaid - boolean,
		dateEnrolled - dateTime
	}

/*

	MongoDB Data Modelling Design

	There are two ways in creating a MongoDB Data Model:
		=> Embedded Data Models
			- generally known as "denormalize" models, and takes advantages of MongoDB's rich documents.
			- subdocument embedded in a parent document.

			example:
				Parent document{
					subdocument{
						
					}
				}

		=> Refference Data Models
			- This are known as "normalize" data models and describes the relationship using reference between documents.
			- Use document ID to connect a document to another document.

*/
